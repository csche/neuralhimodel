load('testSpikeTrain.mat')
nTrails = length(SpTimes_A_plus);
deltaT = 10;
duration = 1;
binWidth = 50e-6;
onsetIgnore = 50e-3;
maxLag = 2e-3;
fs = 100e3;
[TimeBins,Height] = Library.SAC(SpTimes_A_plus,nTrials,deltaT,duration,maxLag,onsetIgnore,fs)