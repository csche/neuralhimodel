%% Define model parameters and generate stimuli
clear all;
fs = 100000; % Sampling frequency
nTrials  = 100; % number of trials for each trial
dt = 1/fs;

%%%%%%%%%%%%%%%%%%%%%%%%% Stimulus A (speech) %%%%%%%%%%%%%%%%%%%%%
A.path = ['..' filesep 'varsha' filesep 'stimuli' filesep];
A.fileExtension = '.wav';
A.prefix = ['quiet' filesep 'quiet'];
A.numberOfSentences = 10;
A.level = 50; %dB SPL according to email from Varsha
A.fs = fs;

%%%%%%%%%%%%%%%%%%%%%%%%% Stimulus B (noisy speech) %%%%%%%%%%%%%%%%%%%%%
B.path = ['..' filesep 'varsha' filesep 'stimuli' filesep];
B.fileExtension = '.wav';
B.quietPrefix = ['quiet' filesep 'quiet'];
B.noisePrefix = ['noise' filesep 'noise'];
B.prefix = ['speechInNoise' filesep 'speechInNoise'];
B.numberOfSentences = 10;
B.level = 50; %dB SPL according to email from Varsha
B.fs = fs;
B.SNR = [-9 -6 -3 0 3 6 9];

%%%%%%%%%%%%%%%%%%%%%%%%% Stimulus N (noise) %%%%%%%%%%%%%%%%%%%%%
N.path = ['..' filesep 'varsha' filesep 'stimuli' filesep];
N.fileExtension = '.wav';
N.prefix = ['noise' filesep 'noise'];
N.numberOfSentences = 10;
N.level = 50; %dB SPL according to email from Varsha
N.fs = fs;

%%%%%%%%%%%%%%%%%% Auditory Nerve Model params %%%%%%%%%%%%%%%
AN.CF = [125 160 200 250 315 400 500 630 800 1000 1250 1600 2000 ...
    2500 3150 4000 5000 6300 8000]; % CF in Hz;
%AN.bml = [25 25 22 22 25];
AN.cohc  = 1.0;    % normal ohc function
AN.cihc  = 1.0;    % normal ihc function
AN.species = 2;    % 1 for cat (2 for human with Shera et al. tuning; 3 for human with Glasberg & Moore tuning)
AN.noiseType = 1;  % 1 for variable fGn (0 for fixed fGn)
AN.fiberType.sr = [2];  % spontaneous rate (in spikes/s) of the fiber BEFORE refractory effects; "1" = Low; "2" = Medium; "3" = High
AN.fiberType.names = {'low','medium','high'}; % names of the fiber types
AN.fiberType.color = {'r','b','g'};
AN.implnt = 0;     % "0" for approximate or "1" for actual implementation of the power-law functions in the Synapse
AN.nTrials = nTrials;

%%%%%%%%%%%%%%%%%%% SAC/SCC Parameters %%%%%%%%%%%%%%%%%%%%
anal.binWidth = 50e-6;
anal.onsetIgnore = 50e-3;
anal.maxLag = 1.5;
anal.maxSpikes = 2500;
anal.fs = fs;
anal.nTrials = nTrials;
anal.plt = 1;

%% Process stimuli with AN model
for cF_i = 1 : length( AN.CF )
    for sentence_i = 1 : A.numberOfSentences
        % Stim A
        [SpT_A_P{cF_i,sentence_i} SpT_A_M{cF_i,sentence_i} duration_A{cF_i,sentence_i}] = Library.getSpikeTrains(A,sentence_i,AN,cF_i);
        
        % Neural Correlograms
        anal.duration = duration_A{cF_i,sentence_i};
        anal.t = -anal.duration+dt:dt:0:dt:anal.duration-dt; % time vector
        results_A{cF_i,sentence_i} = Library.calculateNeuralCorrelograms(SpT_A_P{cF_i,sentence_i},SpT_A_M{cF_i,sentence_i},anal);
        
        % Stim B
        [SpT_B_P{cF_i,sentence_i} SpT_B_M{cF_i,sentence_i} duration_B{cF_i,sentence_i}] = Library.getSpikeTrains(B,sentence_i,AN,cF_i);
        
        % Neural Correlograms
        anal.SNR = B.SNR;
        anal.duration = duration_B{cF_i,sentence_i};
        anal.t = -anal.duration+dt:dt:0:dt:anal.duration-dt; % time vector
        results_B{cF_i,sentence_i} = Library.calculateNeuralCorrelograms(SpT_B_P{cF_i,sentence_i},SpT_B_M{cF_i,sentence_i},anal);
        
        % Stim N
        [SpT_N_P{cF_i,sentence_i} SpT_N_M{cF_i,sentence_i} duration_N{cF_i,sentence_i}] = Library.getSpikeTrains(N,sentence_i,AN,cF_i);
        
        % Neural Correlograms
        anal = rmfield(anal,'SNR');
        anal.duration = duration_N{cF_i,sentence_i};
        anal.t = -anal.duration+dt:dt:0:dt:anal.duration-dt; % time vector
        results_N{cF_i,sentence_i} = Library.calculateNeuralCorrelograms(SpT_N_P{cF_i,sentence_i},SpT_N_M{cF_i,sentence_i},anal);
        
    end
end

save(['speechSpikeTrains_' datestr(now, 'dd-mm-yyyy HH:MM:SS')])