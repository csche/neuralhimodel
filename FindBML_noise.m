% This script will calculate the best modulation level

fs = 100000; % Sampling frequency
nTrials  = 10; % number of trials for each trial
dt = 1/fs;

%%%%%%%%%%%%%%%%%%%%%%%%% Carrier %%%%%%%%%%%%%%%%%%%%%%
C.duration = 1; % duration in seconds
C.rampduration = 0.05; % on/off ramps in seconds
C.silenceduration = 0; % pre and post silence duration in seconds
C.lowfreq = 0; % Low frequency edge
C.highfreq = 50000; % High frequency edge
C.dBNo = [-20:4:80]; % Spectrum level
C.names = {'A_plus','A_minus'};
C.polarity = [1,-1];

%%%%%%%%%%%%%%%%%%%%%%%%% Modulator %%%%%%%%%%%%%%%%%%%%%%
Ms.cf = [25, 50, 100, 200];% 25, 50, 100, 200]; % Modulation frequency
Ms.color = {'r','b','g','k'};
Ms.marker = {'ko-','k^-','ks-','kv-'};
Ms.m = 1; % Modulation depth
Ms.duration = C.duration; % duration in miliseconds
Ms.phase = 0; % Starting phase
Ms.rampduration = 0.05; % on/off ramps in seconds
Ms.silenceduration = 0; % pre and post silence duration in seconds


%%%%%%%%%%%%%%%%%% Auditory Nerve Model params %%%%%%%%%%%%%%%
AN.CF    = [827 4000];   % CF in Hz;
AN.cohc  = 1.0;    % normal ohc function
AN.cihc  = 1.0;    % normal ihc function
AN.species = 1;    % 1 for cat (2 for human with Shera et al. tuning; 3 for human with Glasberg & Moore tuning)
AN.noiseType = 1;  % 1 for variable fGn (0 for fixed fGn)
AN.fiberType.sr = [3];  % spontaneous rate (in spikes/s) of the fiber BEFORE refractory effects; "1" = Low; "2" = Medium; "3" = High
AN.fiberType.names = {'low','medium','high'}; % names of the fiber types
AN.fiberType.color = {'r','b','g'};
AN.implnt = 0;     % "0" for approximate or "1" for actual implementation of the power-law functions in the Synapse
AN.duration = C.duration*1; % AN model is run for 1.1 times the duration of signal
AN.times = 0:1/fs:AN.duration*2-1/fs; % make time array for AN spike times

%%%%%%%%%%%%%%%%%%% SAC/SCC Parameters %%%%%%%%%%%%%%%%%%%%
anal.Duration = 1;
anal.t = -anal.Duration+dt:dt:0:dt:anal.Duration-dt; % time vector
anal.BinWidth = 50e-6;
anal.RemoveOnset = 50e-3;
anal.MaxLag = 2e-3;

%%%%%%%%%%%%%%%%%% Set figure defaults %%%%%%%%%%%%%%%%%%%%
set(0,'DefaultAxesFontSize',14)
set(0,'DefaultTextFontSize', 14)
hFig = figure
pltWaveForm =1;
%% First run noise alone
for iANCF = 1:length(AN.CF)
    AN_CF = AN.CF(iANCF);
    
    subplot(length(AN.CF),1,iANCF);
    hold on
    box on;
    
    for iSR = 1:length(AN.fiberType.sr)
        whatSR = AN.fiberType.sr(iSR);
        
        for iModCF = 1:length(Ms.cf)
            modCF = Ms.cf(iModCF);
            ModSig = Library.msine(modCF,Ms.duration,Ms.phase,Ms.rampduration,Ms.silenceduration,fs);

            tempData = [];
            for idB = 1:length(C.dBNo)
                dBNo = C.dBNo(idB);
                
                %%%%%% Make carrier
                CarrierWave = Library.gnoise(C.lowfreq,C.highfreq,C.duration,dBNo,C.rampduration,C.silenceduration,fs);

                % A_plus
                pin = CarrierWave.*(1+(Ms.m*ModSig));
                if pltWaveForm
                   figure
                   %subplot(2,1,1)
                   tVec = 1:length(pin);
                   tVec = (tVec-1).*dt;
                   plot(tVec(1:end/2),pin(1:end/2)')
                   title('A-')
                   xlabel('t [s]');
                   ylabel('Amplitude');
                end
                SpTimes_A_Plus = {};
                %%%%%% Run peripheral model
                vihc = ANModel.model_IHC(pin',AN_CF,1,1/fs,AN.duration,AN.cohc,AN.cihc,AN.species);
                parfor iTrial = 1:nTrials
                    %%%%%% Run the auditory nerve model
                    [~,~,psth] = ANModel.model_Synapse(vihc,AN_CF,1,1/fs,whatSR,AN.noiseType,AN.implnt);
                    idx = psth==1;
                    data = AN.times(idx);
                    %%%%%%%%%%% Save data %%%%%%%%%%%
                    SpTimes_A_Plus{iTrial} = data;
                end
                
                % A_minus
                pin = -CarrierWave.*(1+(Ms.m*ModSig));
                if pltWaveForm
%                    subplot(2,1,2)
%                    plot(pin')
%                    title('A-')
%          
                   saveFigureAs(['waveFormModCf_' num2str(modCF) '_dB' num2str(dBNo) '.eps'])
                end
                SpTimes_A_minus = {};
                %%%%%% Run peripheral model
                vihc = ANModel.model_IHC(pin',AN_CF,1,1/fs,AN.duration,AN.cohc,AN.cihc,AN.species);
                parfor iTrial = 1:nTrials
                    %%%%%% Run the auditory nerve model
                    [~,~,psth] = ANModel.model_Synapse(vihc,AN_CF,1,1/fs,whatSR,AN.noiseType,AN.implnt);
                    idx = psth==1;
                    data = AN.times(idx);
                    %%%%%%%%%%% Save data %%%%%%%%%%%
                    SpTimes_A_minus{iTrial} = data;
                end
                
                % Calculate SACs
                [~,SAC1]=Library.SAC(SpTimes_A_Plus,nTrials,anal.BinWidth,anal.Duration,anal.MaxLag,anal.RemoveOnset,fs);
                [~,SAC2]=Library.SAC(SpTimes_A_minus,nTrials,anal.BinWidth,anal.Duration,anal.MaxLag,anal.RemoveOnset,fs);
                % Calculate SCCs
                [~,SCC1]=Library.SCC(SpTimes_A_Plus,SpTimes_A_minus,nTrials,anal.BinWidth,anal.Duration,anal.MaxLag,anal.RemoveOnset,fs);
                [TimeBins,SCC2]=Library.SCC(SpTimes_A_Plus,SpTimes_A_minus,nTrials,anal.BinWidth,anal.Duration,anal.MaxLag,anal.RemoveOnset,fs);

                % Calculate sumcor
                sumcor1 = mean([SAC1; SCC1]);
                sumcor2 = mean([SAC2; SCC2]);
                maxSUMCOR1 = sumcor1(TimeBins==0);
                maxSUMCOR2 = sumcor2(TimeBins==0);
                
                tempData = [tempData; mean([maxSUMCOR1,maxSUMCOR2])];
            end
            set(0, 'currentfigure', hFig);
            plot(C.dBNo,tempData,Ms.marker{iModCF},'MarkerFaceColor',Ms.color{iModCF})
            legend(cellstr(num2str(Ms.cf', 'f_M=%-d')))
            ylim([1,3.5])
            title(['Level vs Fir ing-Rate, CF=' num2str(AN_CF) ' Hz'])
            ylabel('Max Sumcor')
            xlabel('Level [dB]')
            pause(0.001)
        end
    end
end
saveFigureAs('cf_827and4000_boradband_noise_10trails.eps')