% Shuffled auto- and cross-correlogram analyses are used to compute
% separate correlations for responses to envelope and fine structure based
% on both model spike trains from auditory nerve fibers.
%
% According to Heinz, M. G., & Swaminathan, J. (2009). Quantifying envelope
%       and fine-structure coding in auditory nerve responses to chimaeric
%       speech. Journal of the Association for Research in Otolaryngology:
%       JARO, 10(3), 407–23. doi:10.1007/s10162-009-0169-8



%% Define model parameters and generate stimuli
clear all;
fs = 100000; % Sampling frequency
nTrials  = 49; % number of trials for each trial
dt = 1/fs;

%%%%%%%%%%%%%%%%%%%%%%%%% Carrier %%%%%%%%%%%%%%%%%%%%%%
C.duration = 1; % duration in seconds
C.rampduration = 0.05; % on/off ramps in seconds
C.silenceduration = 0; % pre and post silence duration in seconds
C.freq = 1e3; % Low frequency edge
C.dBNo = [-20:4:80]; % Spectrum level
C.names = {'A_plus','A_minus'};
C.polarity = [1,-1];
C.phase = 0;
C.rampduration = 0.05; % on/off ramps in seconds
C.silenceduration = 0; % pre and post silence duration in seconds

%%%%%%%%%%%%%%%%%%%%%%%%% Modulator %%%%%%%%%%%%%%%%%%%%%%
Ms.fm = [10 100];% 25, 50, 100, 200]; % Modulation frequency
Ms.color = {'r','b','g','k'};
Ms.marker = {'ko-','k^-','ks-','kv-'};
Ms.m = 1; % Modulation depth
Ms.duration = C.duration; % duration in miliseconds
Ms.phase = 0; % Starting phase
Ms.rampduration = 0.05; % on/off ramps in seconds
Ms.silenceduration = 0; % pre and post silence duration in seconds


%%%%%%%%%%%%%%%%%% Auditory Nerve Model params %%%%%%%%%%%%%%%
fMin=80; fMax=8000; nBand=5;

fCoSignal = Library.equalXbmBands(fMin, fMax, nBand);
for kk = 1 : nBand
    CFs(kk) = round( fCoSignal(kk) +(fCoSignal(kk+1)-fCoSignal(kk))/2);
end

AN.CF    = CFs;   % CF in Hz;
AN.CF = [900 990 1000 1010 1100];
AN.cohc  = 1.0;    % normal ohc function
AN.cihc  = 1.0;    % normal ihc function
AN.species = 1;    % 1 for cat (2 for human with Shera et al. tuning; 3 for human with Glasberg & Moore tuning)
AN.noiseType = 1;  % 1 for variable fGn (0 for fixed fGn)
AN.fiberType.sr = [3];  % spontaneous rate (in spikes/s) of the fiber BEFORE refractory effects; "1" = Low; "2" = Medium; "3" = High
AN.fiberType.names = {'low','medium','high'}; % names of the fiber types
AN.fiberType.color = {'r','b','g'};
AN.implnt = 0;     % "0" for approximate or "1" for actual implementation of the power-law functions in the Synapse
AN.duration = C.duration*1; % AN model is run for 1 times the duration of signal
AN.times = 0:1/fs:AN.duration*2-1/fs; % make time array for AN spike times

%%%%%%%%%%%%%%%%%%% SAC/SCC Parameters %%%%%%%%%%%%%%%%%%%%
anal.duration = 1;
anal.t = -anal.duration+dt:dt:0:dt:anal.duration-dt; % time vector
anal.binWidth = 50e-6;
anal.onsetIgnore = 50e-3;
anal.maxLag = 2e-3;
anal.maxSpikes = 3600;
anal.fs = fs;
anal.nTrials = nTrials;


%%%%%%%%%%%%%%%%%% Set figure defaults %%%%%%%%%%%%%%%%%%%%
plt = 0;

if plt
    set(0,'DefaultAxesFontSize',10)
    set(0,'DefaultTextFontSize', 10)
    figure
    subplot(331);
    %text(.75,1.25,['Neural Correlations at ' num2str(B.snr) 'dB SNR'])
end

levels = -20:5:150;

%% Process stimuli with AN model

dBNo = 100;

for fm_i = 1 : length(Ms.fm)

fm = Ms.fm(fm_i);
modSig = Library.msine(fm,Ms.duration,Ms.phase,Ms.rampduration,Ms.silenceduration,fs);
carrier = Library.msine(C.freq,C.duration,C.phase,C.rampduration,C.silenceduration,fs);;

% Stimulus A
A.fs = fs;
A.name = ['modulatedPT1kHz_fm' num2str(fm)];
A.wav = carrier .* (1+(Ms.m*modSig));
A.duration = C.duration;
if 0
    figure
    %subplot(2,1,1)
    tVec = 0:dt:A.duration-dt;
    plot(tVec,A.wav)
    title('A-')
    xlabel('t [s]');
    ylabel('Amplitude');
end

for cF_i = 1 : length( AN.CF )
    for level_i = 1 : length(levels)
        level = levels(level_i);
        
        A.dBSPL=20*log10(sqrt(mean(A.wav.^2))/(20e-6));
        
        A.wavLevelAdj=A.wav*10^((level-A.dBSPL)/20);
        
        % A_plus
        pin = A.wavLevelAdj;
        SpTimes_A_Plus = {};
        %%%%%% Run peripheral model
        vihc = ANModel.model_IHC(pin',AN.CF(cF_i),1,1/fs,AN.duration,AN.cohc,AN.cihc,AN.species);
        parfor iTrial = 1:nTrials
            %%%%%% Run the auditory nerve model
            [~,~,psth] = ANModel.model_Synapse(vihc,AN.CF(cF_i),1,1/fs,AN.fiberType.sr,AN.noiseType,AN.implnt);
            idx = psth==1;
            data = AN.times(idx);
            %%%%%%%%%%% Save data %%%%%%%%%%%
            SpTimes_A_Plus{iTrial} = data;
        end
        firingRate_A_plus(cF_i,level_i) = Library.calculateFiringRate(SpTimes_A_Plus,anal);
        
        % A_minus
        pin = -A.wavLevelAdj;
        SpTimes_A_minus = {};
        %%%%%% Run peripheral model
        vihc = ANModel.model_IHC(pin',AN.CF(cF_i),1,1/fs,AN.duration,AN.cohc,AN.cihc,AN.species);
        parfor iTrial = 1:nTrials
            %%%%%% Run the auditory nerve model
            [~,~,psth] = ANModel.model_Synapse(vihc,AN.CF(cF_i),1,1/fs,AN.fiberType.sr,AN.noiseType,AN.implnt);
            idx = psth==1;
            data = AN.times(idx);
            %%%%%%%%%%% Save data %%%%%%%%%%%
            SpTimes_A_minus{iTrial} = data;
        end
        
        firingRate_A_minus(cF_i,level_i) = Library.calculateFiringRate(SpTimes_A_minus,anal);
        
        results = Library.calculateNeuralCorrelograms(SpTimes_A_Plus,SpTimes_A_minus,anal);
        avgFR_A(cF_i,level_i) = mean([firingRate_A_plus(cF_i,level_i) firingRate_A_minus(cF_i,level_i)]);
        sumcorPeak_A(cF_i,level_i) = results.maxSUMCOR_A;
    end
end

bmlData.name = A.name;
bmlData.levels = levels;
bmlData.CF = AN.CF;
bmlData.avgFR_A = avgFR_A;
bmlData.sumcorPeak_A = sumcorPeak_A;



%% SAVE BML Data
%fig_filename=strcat(DATAfilename{nstim},'_',num2str(CFs_Hz(cflength)));
filename=['bmlData' A.name '_' Ms.fm(fm_i) '.mat'];

%disp(sprintf('SAVING BML Data: %s_%d_BML.mat',DATAfilename{nstim},CFs_Hz(cflength)))
%disp(sprintf('SAVING BML Data: %s_%d_BML.mat',DATAfilename{nstim},(cflength)))

save(filename, 'bmlData')
Library.plotLevelBmlFiringRate(filename)
clear bmlData
end

close all
