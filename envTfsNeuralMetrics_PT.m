% Shuffled auto- and cross-correlogram analyses are used to compute
% separate correlations for responses to envelope and fine structure based
% on both model spike trains from auditory nerve fibers.
%
% According to Heinz, M. G., & Swaminathan, J. (2009). Quantifying envelope
%       and fine-structure coding in auditory nerve responses to chimaeric
%       speech. Journal of the Association for Research in Otolaryngology:
%       JARO, 10(3), 407–23. doi:10.1007/s10162-009-0169-8



%% Define model parameters and generate stimuli
clear all;
fs = 100000; % Sampling frequency
nTrials  = 49; % number of trials for each trial
dt = 1/fs;

%%%%%%%%%%%%%%%%%%%%%%%%% Stimulus A (s) %%%%%%%%%%%%%%%%%%%%%
A.fileName = 'speech.wav';
A.name = 'speech';
A.fs = fs;
A.wav = Library.getWav(A.fileName, A.fs);
A.duration = length(A.wav)*1/A.fs;

%%%%%%%%%%%%%%%%%%%%%%%%% Stimulus B (s+ssn) %%%%%%%%%%%%%%%%%
B.fs = A.fs;
B.speech = A.wav;
B.snr = 0;
B.noise = Library.getSSN(B.speech, B.snr, B.fs);
B.wav = B.speech + B.noise;
B.duration = length(B.wav)*1/B.fs;


%%%%%%%%%%%%%%%%%% Auditory Nerve Model params %%%%%%%%%%%%%%%
fMin=80; fMax=8000; nBand=5;

fCoSignal = Library.equalXbmBands(fMin, fMax, nBand);
for kk = 1 : nBand
    CFs(kk) = round( fCoSignal(kk) +(fCoSignal(kk+1)-fCoSignal(kk))/2);
end

AN.CF    = CFs;   % CF in Hz;
%AN.CF = 825;
AN.bml = [70 58 58 46 57];
AN.cohc  = 1.0;    % normal ohc function
AN.cihc  = 1.0;    % normal ihc function
AN.species = 1;    % 1 for cat (2 for human with Shera et al. tuning; 3 for human with Glasberg & Moore tuning)
AN.noiseType = 1;  % 1 for variable fGn (0 for fixed fGn)
AN.fiberType.sr = [3];  % spontaneous rate (in spikes/s) of the fiber BEFORE refractory effects; "1" = Low; "2" = Medium; "3" = High
AN.fiberType.names = {'low','medium','high'}; % names of the fiber types
AN.fiberType.color = {'r','b','g'};
AN.implnt = 0;     % "0" for approximate or "1" for actual implementation of the power-law functions in the Synapse
AN.duration = A.duration*1; % AN model is run for 1 times the duration of signal
AN.times = 0:1/fs:AN.duration*2-1/fs; % make time array for AN spike times

%%%%%%%%%%%%%%%%%%% SAC/SCC Parameters %%%%%%%%%%%%%%%%%%%%
anal.duration = 1;
anal.t = -anal.duration+dt:dt:0:dt:anal.duration-dt; % time vector
anal.binWidth = 50e-6;
anal.onsetIgnore = 50e-3;
anal.maxLag = 2e-3;
anal.maxSpikes = 3600;
anal.fs = fs;
anal.nTrials = nTrials;
anal.plt = 1;


%%%%%%%%%%%%%%%%%% Set figure defaults %%%%%%%%%%%%%%%%%%%%
plt = 0;

if plt
    set(0,'DefaultAxesFontSize',10)
    set(0,'DefaultTextFontSize', 10)
    figure
    subplot(331);
    %text(.75,1.25,['Neural Correlations at ' num2str(B.snr) 'dB SNR'])
end

levels = -20:5:150;

%% Process stimuli with AN model
for cF_i = 1 : length( AN.CF )
    level = AN.bml(cF_i);
    
    A.dBSPL=20*log10(sqrt(mean(A.wav.^2))/(20e-6));
    A.wavLevelAdj=A.wav*10^((level-A.dBSPL)/20);
    B.dBSPL=20*log10(sqrt(mean(B.wav.^2))/(20e-6));
    B.wavLevelAdj=B.wav*10^((level-B.dBSPL)/20);
    
    % A_plus
    pin = A.wavLevelAdj;
    SpTimes_A_plus = {};
    %%%%%% Run peripheral model
    vihc = ANModel.model_IHC(pin',AN.CF(cF_i),1,1/fs,AN.duration,AN.cohc,AN.cihc,AN.species);
    parfor iTrial = 1:nTrials
        %%%%%% Run the auditory nerve model
        [~,~,psth] = ANModel.model_Synapse(vihc,AN.CF(cF_i),1,1/fs,AN.fiberType.sr,AN.noiseType,AN.implnt);
        idx = psth==1;
        data = AN.times(idx);
        %%%%%%%%%%% Save data %%%%%%%%%%%
        SpTimes_A_plus{iTrial} = data;
    end
    %firingRate_A_plus(cF_i,level_i) = Library.calculateFiringRate(SpTimes_A_plus,anal);
    
    % A_minus
    pin = -A.wavLevelAdj;
    SpTimes_A_minus = {};
    %%%%%% Run peripheral model
    vihc = ANModel.model_IHC(pin',AN.CF(cF_i),1,1/fs,AN.duration,AN.cohc,AN.cihc,AN.species);
    parfor iTrial = 1:nTrials
        %%%%%% Run the auditory nerve model
        [~,~,psth] = ANModel.model_Synapse(vihc,AN.CF(cF_i),1,1/fs,AN.fiberType.sr,AN.noiseType,AN.implnt);
        idx = psth==1;
        data = AN.times(idx);
        %%%%%%%%%%% Save data %%%%%%%%%%%
        SpTimes_A_minus{iTrial} = data;
    end
    
    %firingRate_A_minus(cF_i,level_i) = Library.calculateFiringRate(SpTimes_A_minus,anal);
    
    % B_plus
    pin = B.wavLevelAdj;
    SpTimes_B_plus = {};
    %%%%%% Run peripheral model
    vihc = ANModel.model_IHC(pin',AN.CF(cF_i),1,1/fs,AN.duration,AN.cohc,AN.cihc,AN.species);
    parfor iTrial = 1:nTrials
        %%%%%% Run the auditory nerve model
        [~,~,psth] = ANModel.model_Synapse(vihc,AN.CF(cF_i),1,1/fs,AN.fiberType.sr,AN.noiseType,AN.implnt);
        idx = psth==1;
        data = AN.times(idx);
        %%%%%%%%%%% Save data %%%%%%%%%%%
        SpTimes_B_plus{iTrial} = data;
    end
    %firingRate_A_plus(cF_i,level_i) = Library.calculateFiringRate(SpTimes_A_plus,anal);
    
    % A_minus
    pin = -B.wavLevelAdj;
    SpTimes_B_minus = {};
    %%%%%% Run peripheral model
    vihc = ANModel.model_IHC(pin',AN.CF(cF_i),1,1/fs,AN.duration,AN.cohc,AN.cihc,AN.species);
    parfor iTrial = 1:nTrials
        %%%%%% Run the auditory nerve model
        [~,~,psth] = ANModel.model_Synapse(vihc,AN.CF(cF_i),1,1/fs,AN.fiberType.sr,AN.noiseType,AN.implnt);
        idx = psth==1;
        data = AN.times(idx);
        %%%%%%%%%%% Save data %%%%%%%%%%%
        SpTimes_B_minus{iTrial} = data;
    end
    anal.CF = AN.CF(cF_i);
    anal.snr = B.snr;
    anal.name = A.name;
    results = Library.calculateCrossNeuralCorrelograms(SpTimes_A_plus,SpTimes_A_minus,SpTimes_B_plus,SpTimes_B_minus,anal);
end
