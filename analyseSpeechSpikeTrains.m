clear all;
load('speechSpikeTrains_11-11-2015 11:15:50.mat')

SUMCOR = results_A{1,1}.SUMCOR

% analyze SACs
params.DELAYbinwidth_sec=anal.binWidth;
params.CF_A_Hz = AN.CF(1);
% FFT window length - to get 1-Hz sampling
Nfft_psd=round(1/params.DELAYbinwidth_sec);  % 20000
params.Nfft_psd=Nfft_psd;

% Remove TFS artifact (centered at 2*CF) from SUMCOR
% zero out above CF
FFTtemp=fft((SUMCOR-1),params.Nfft_psd);
freqVEC=(0:length(FFTtemp)-1)/length(FFTtemp)*params.Nfft_psd;
[y,CF_index]=min(abs(freqVEC-params.CF_A_Hz)); % use CF_A
FFTadj=zeros(size(FFTtemp));
FFTadj(1:CF_index)=FFTtemp(1:CF_index);
FFTadj((length(FFTtemp)-CF_index+1):end)=FFTtemp((length(FFTtemp)-CF_index+1):end); %keep negative freqs
adjSC=ifft(FFTadj)+1;
SUMCORadj_A=real(adjSC(1:length(SUMCOR)));

%% Compute ENVELOPE POWER SPECTRAL DENSITY
PSDsc_A=abs(FFTtemp);

% PSD/CSD summations - Compute summed energy in ENVELOPE spectral
% densities over various frequency ranges

% default list
default_PSD_LHfreqs_Hz=[0 1; 1 2; 2 4; 4 8; 8 16; 16 32; 32 64];

params.PSD_LHfreqs_Hz=default_PSD_LHfreqs_Hz;


% find INDs in freqVEC_Hz for relevant cutoffs
for i=1:size(params.PSD_LHfreqs_Hz,1)
    [y,PSD_LHfreqs_inds(i,1)]=min(abs(freqVEC-params.PSD_LHfreqs_Hz(i,1)));
    [y,PSD_LHfreqs_inds(i,2)]=min(abs(freqVEC-params.PSD_LHfreqs_Hz(i,2)));
end

PSDsc_A=abs(fft((SUMCOR-1),params.Nfft_psd));

% Compute all sums for all spectral densities
for i=1:size(params.PSD_LHfreqs_Hz,1)
    sumPSD(i) = sum(PSDsc_A(PSD_LHfreqs_inds(i,1):PSD_LHfreqs_inds(i,2)));
end

% divide PSD by FFT length to obtain power
sumPSD = sumPSD/params.Nfft_psd

% plot
XLIMIT_delay=3;
XLIMIT_PSDhigh=500;
YLIMIT_SClow=0.9;
YLIMIT_PSD=max([max(PSDsc_A) ]) %max(PSDsc_B) max(CSDsc_AB)]);

CCCenv_TOUSE='adjSC';
CCCenvs_legend{1}='rawSC';
CCCenvs_legend{end+1}='adjSC';
CCCenvs_legend{end+1}='IFFTrawSC';
CCCenvs_legend{end+1}='IFFTrawSC_0';
CCCenvs_legend{end+1}='IFFTrawSC_CD';
CCCenv_TOUSEindex=find(strcmp(CCCenv_TOUSE,CCCenvs_legend));

CCCenv_LOWmod_Hz = params.PSD_LHfreqs_Hz(ceil(CCCenv_TOUSEindex/2),1)
CCCenv_HIGHmod_Hz = params.PSD_LHfreqs_Hz(ceil(CCCenv_TOUSEindex/2),2);

figure
plot(freqVEC,PSDsc_A,'k');  xlim([0 XLIMIT_PSDhigh]); ylim([0 YLIMIT_PSD]); hold on
plot(ones(1,2)*CCCenv_LOWmod_Hz,[0 YLIMIT_PSD],'--k','linewidth',2);
plot(ones(1,2)*CCCenv_HIGHmod_Hz,[0 YLIMIT_PSD],'--k','linewidth',2); hold off
set(gca, 'Box', 'off', 'TickDir', 'out');
ylabel(sprintf('SPECTRAL DENSITY\nAMPLITUDE'),'FontSize',12,'HorizontalAlignment','center')
text(1.2*CCCenv_LOWmod_Hz,YLIMIT_PSD,sprintf('%.f',5),'units','data','VerticalAlignment','top','HorizontalAlignment','left','FontSize',10)

figure
plot(freqVEC,PSDsc_B,'k');  xlim([0 XLIMIT_PSDhigh]); ylim([0 YLIMIT_PSD]); hold on
plot(ones(1,2)*paramsIN.CCCenv_LOWmod_Hz,[0 YLIMIT_PSD],'--k','linewidth',2);
plot(ones(1,2)*paramsIN.CCCenv_HIGHmod_Hz,[0 YLIMIT_PSD],'--k','linewidth',2); hold off
set(gca, 'Box', 'off', 'TickDir', 'out');
text(1.2*paramsIN.CCCenv_LOWmod_Hz,YLIMIT_PSD,sprintf('%.f',SACSCCmetrics.sums.sumPSDsc_B_CCCenv),'units','data','VerticalAlignment','top','HorizontalAlignment','left','FontSize',10)

figure;
plot(freqVEC,CSDsc_AB,'k');  xlim([0 XLIMIT_PSDhigh]); ylim([0 YLIMIT_PSD]); hold on
plot(ones(1,2)*paramsIN.CCCenv_LOWmod_Hz,[0 YLIMIT_PSD],'--k','linewidth',2);
plot(ones(1,2)*paramsIN.CCCenv_HIGHmod_Hz,[0 YLIMIT_PSD],'--k','linewidth',2); hold off
set(gca, 'Box', 'off', 'TickDir', 'out');
text(1,1,sprintf('CCCenv=%.2f',SACSCCmetrics.CCCenv),'units','norm','VerticalAlignment','top','HorizontalAlignment','right','FontSize',12,'Color','red')
text(1.2*paramsIN.CCCenv_LOWmod_Hz,YLIMIT_PSD,sprintf('%.f',SACSCCmetrics.sums.sumCSDsc_AB_CCCenv),'units','data','VerticalAlignment','top','HorizontalAlignment','left','FontSize',10)
	